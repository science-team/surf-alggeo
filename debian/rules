#!/usr/bin/make -f
include /usr/share/dpkg/pkg-info.mk

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

## frozen to the C++14 standard plus GNU extensions
## as the upstream source is no more maintained (2021/10/12).
CXXFLAGS += -std=gnu++14

DEB_VIRT_SURFALGGEO_LISTOF_VARIANT = nox ##gtk

CONFIGURE_OPTS_nox = --disable-gui --disable-cups
##CONFIGURE_OPTS_gtk = --enable-gui --enable-cups

## inspired from gsl material
ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -O0
else ifeq ($(DEB_HOST_ARCH), m68k)
	CFLAGS += -O2
else ifeq ($(DEB_HOST_ARCH), alpha)
	CFLAGS += -O2 -mieee
else
	CFLAGS += -O3
endif

default:
	@uscan --no-conf --dehs --report || true

%:
	dh $@ --no-parallel

override_dh_auto_configure:
	$(foreach vt, $(DEB_VIRT_SURFALGGEO_LISTOF_VARIANT), \
		dh_auto_configure -B _build/$(vt) -- --srcdir=../.. --program-suffix=-alggeo-$(vt) $(CONFIGURE_OPTS_$(subst -,_,$(vt))) ;\
		)

override_dh_auto_build-arch:
	$(foreach vt, $(DEB_VIRT_SURFALGGEO_LISTOF_VARIANT), dh_auto_build -a -B _build/$(vt) ;)

override_dh_auto_build-indep:
	$(MAKE) -C docs all

override_dh_auto_install-arch:
	$(foreach vt, $(DEB_VIRT_SURFALGGEO_LISTOF_VARIANT), dh_auto_install -a -B _build/$(vt) ;)

override_dh_compress-indep:
	dh_compress -X.pdf -Xexamples

override_dh_missing:
	dh_missing -Xsurf.xpm

override_dh_auto_clean-arch:
	dh_auto_clean -a
	rm -rf _build
